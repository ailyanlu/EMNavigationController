//
//  EMTabViewController.h
//  EMNavigationController
//
//  Created by EasonWang on 13-11-25.
//  Copyright (c) 2013年 EasonWang. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface EMTabViewController : NSObject

/**
 获取与tabbar关联的viewControllers
 */
+(NSArray*)getTabViewControllers;

@end
