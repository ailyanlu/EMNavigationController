//
//  AutoDismissAlertView.m
//  LLYG
//
//  Created by EasonWang on 13-9-24.
//  Copyright (c) 2013年 ShiTengTechnology. All rights reserved.
//

#import "AutoDismissAlertView.h"


@implementation AutoDismissAlertView

-(id) initWithTitle:(NSString *)title message:(NSString *)message delegate:(id<UIAlertViewDelegate>)delegate dismissAfterSeconds:(NSUInteger)seconds animated:(BOOL)animated{
    self = [super initWithTitle:title message:message delegate:delegate cancelButtonTitle:nil otherButtonTitles:nil];
    if (self) {
        mAnimated = animated;
        mDelaySeconds = seconds;
    }
    return self;
}

-(void) show {
    [super show];
    [self performSelector:@selector(dismiss) withObject:nil afterDelay:mDelaySeconds];
}


-(void) dismiss {
    [super dismissWithClickedButtonIndex:0 animated:mAnimated];
}

@end
