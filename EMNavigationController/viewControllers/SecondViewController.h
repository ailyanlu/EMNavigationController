//
//  SecondViewController.h
//  EMultiNavigation
//
//  Created by EasonWang on 13-11-10.
//  Copyright (c) 2013年 EasonWang. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "EMViewController.h"
@interface SecondViewController : EMViewController


-(IBAction)pushBut:(id)sender;
@end
